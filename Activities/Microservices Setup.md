# Microservices Setup

The Microservices activites are intended to provide onboarding to those wanting to understand how to work within the microservices architecture that is being used by LibreFoodPantry. The activities are developed around a version of the
[Thea's Pantry GuestInfoSystem](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem).  The Bear Necessities Market project has been developed in parallel with Thea's Pantry and the code is very similar. The "Kit" that you will be working with contains:  
 * code repositories, each "captured" at a particular commit
 * documentation and issue repositories, each "captured" at a particular commit
 * the activities repository, "captured" at a particular commit

You already have a team copy of the code repositories, documentation and issue repositories in your team folder. In order to complete the Microservices activities as well as to prepare for future homeworks, you must complete the actions below.  The first actions you must complete as a **team** and the second set of activities you must complete as an **individual**: 

## Team Activity
You will need a single copy of the activities per team. We will be completing these activities in class (and after class if the activities are not completed within class). Do the following: 
 * As a team, fork a copy of the Microservices Activities from https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microservices-activities
 * You must fork into your team's group in https://gitlab.com/LibreFoodPantry/training/wne-cs-490-fall-2024

## Individual Activity
We will be having some homework based on the micorservices activities. These homeworks will be completed on GitLab. Therefore, each individual needs a private project in which to complete the homeworks. Do the following: 
 * Each individual must create a project titled **Homework - <lastname>** within your team's group in https://gitlab.com/LibreFoodPantry/training/wne-cs-490-fall-2024
 * Make the access to this project **private**

 
