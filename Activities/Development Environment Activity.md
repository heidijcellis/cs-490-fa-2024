# Development Environment Activity

This activity is intended to allow you to become familiar with VSCode and Dev Containers.  Every team member will need to have the development environment set up on their machine and become familiar with using it. Therefore each **individual** should do the following within your team: 

1. Start Docker Desktop 
2. Go to: [Development Environment project](https://gitlab.com/heidijcellis/development-environments/-/blob/main/DevelopmentEnvironments.md?ref_type=heads)
3. Clone the project to your own machine and open in VSCode using the  Code button and the “Visual Studio Code (HTTPS)” option – You will need to enter your GitLab user name and password.
4. Click 'Open in Dev Container'.
5. Install the recommended extensions for the project. You’ll need these to perform some of the activities. 
6. Complete the activities in the DevelopmentEnvironments.md file. 

Be careful of using keyboard shortcuts in VSCode as some have different definitions within VSCode, Control-c and Control-v in particular. 